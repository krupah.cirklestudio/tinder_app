class UserArguments {
  String? name;
  String? email;
  String? gender;
  String? imageURL;
  bool? logIN;

  UserArguments({this.name, this.email, this.gender, this.imageURL, this.logIN});
}
