class ProfilePicture {
  final String? imageURL;
  final String? userName;
  final String? gender;
  final String? id;
  final bool? isFavourite;

  ProfilePicture(this.imageURL, this.userName, this.gender, this.id, this.isFavourite);
}
