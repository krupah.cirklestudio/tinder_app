class Routes {
  static const String splashScreen = 'SplashScreen';
  static const String loginScreen = 'LoginScreen';
  static const String signupScreen = 'SignupScreen';
  static const String allScreenBottom = 'AllScreenBottom';
  static const String profileScreen = 'ProfileScreen';
  static const String settingScreen = 'SettingScreen';
  static const String homeScreen = 'HomeScreen';
  static const String addMediaScreen = 'AddMediaScreen';
  static const String editProfileScreen = 'EditProfileScreen';
}

