import 'package:flutter/material.dart';

import 'color_constant.dart';

class TextStyleConstant {
  static const TextStyle sigUpStyle =
      TextStyle(color: ColorConstant.white, fontSize: 16, letterSpacing: 1, fontWeight: FontWeight.w500);
  static const TextStyle logInBtnStyle =
      TextStyle(color: ColorConstant.pink, fontSize: 16, fontWeight: FontWeight.w500);
  static const TextStyle settingNameStyle =
      TextStyle(color: ColorConstant.black, fontSize: 22, fontWeight: FontWeight.bold);
  static const TextStyle settingEmailStyle = TextStyle(color: ColorConstant.black, fontSize: 19);
}
