class ImageConstant {
  static const String splashLogo = 'assets/image/login_logo.png';
  static const String add = 'assets/image/add.png';
  static const String user = 'assets/image/user.jpeg';
  static const String addUser = 'assets/image/add-user.png';
  static const String camera = 'assets/image/plus.png';
  static const String check = 'assets/image/check.png';
}